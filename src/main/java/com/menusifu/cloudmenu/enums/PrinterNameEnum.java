package com.menusifu.cloudmenu.enums;

/**
 * @author xie
 * @date 2019/1/9 16:34
 * @description:
 */
public enum PrinterNameEnum {
    Kitchen, Sushi, Runner, Receipt;

    @Override
    public String toString() {
        return super.toString().toUpperCase();
    }
}
