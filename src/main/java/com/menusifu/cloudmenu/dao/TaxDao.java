package com.menusifu.cloudmenu.dao;

import com.menusifu.cloudmenu.entity.Tax;

import java.util.List;

/**
 * @author xie
 * @date 2019/1/15 14:12
 * @description:
 */
public interface TaxDao {
    List<Tax> getAllTaxes();

    Tax addTax(Tax Tax);

    void updateTax(Tax Tax);

    void deleteTax(String id);

    Tax getTaxById(String id);
}
