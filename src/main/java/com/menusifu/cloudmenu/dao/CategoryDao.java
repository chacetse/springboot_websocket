package com.menusifu.cloudmenu.dao;

import com.menusifu.cloudmenu.entity.Category;

import java.util.List;

/**
 * @author xie
 * @date 2019/1/15 14:35
 * @description:
 */
public interface CategoryDao {
    List<Category> getAllCategories();

    Category addCategory(Category Category);

    void updateCategory(Category Category);

    void deleteCategory(String id);

    Category getCategoryById(String id);

    List<Category> getCategoriesByTaxId(String taxId);
}
