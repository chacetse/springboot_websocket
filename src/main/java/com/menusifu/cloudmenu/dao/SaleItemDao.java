package com.menusifu.cloudmenu.dao;

import com.menusifu.cloudmenu.entity.SaleItem;

import java.util.List;

/**
 * @author xie
 * @date 2019/1/9 16:48
 * @description:
 */
public interface SaleItemDao {
    List<SaleItem> getAllSaleItems();

    SaleItem addSaleItem(SaleItem saleItem);

    void updateSaleItem(SaleItem saleItem);

    void deleteSaleItemById(String id);

    SaleItem getSaleItemById(String id);

    List<SaleItem> batchAddSaleItems(List<SaleItem> saleItems);

    List<SaleItem> getSaleItemsByCategoryId(String categoryId);

    void deleteSaleItemByCatId(String categoryId);
}
