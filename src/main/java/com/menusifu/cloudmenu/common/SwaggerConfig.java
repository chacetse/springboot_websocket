package com.menusifu.cloudmenu.common;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author xie
 * @date 2019/1/10 9:15
 * @description:
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Value("${swagger.enable}")
    private Boolean enable ;
    @Bean
    public Docket docket(){
        return new Docket(DocumentationType.SWAGGER_2)
                .enable(enable)
                .apiInfo(apiInfo()).select()
                //Current package path
                .apis(RequestHandlerSelectors.basePackage("com.menusifu.cloudmenu.web"))
                .paths(PathSelectors.any()).build();

    }
    //Build the details function of the api document
    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                //page title
                .title("Menu Sync Restful API")
                //founder
                .contact(new Contact("menusifu menu sync", "", ""))
                //version number
                .version("1.0")
                //description
                .description("API Desc")
                .build();
    }
}
