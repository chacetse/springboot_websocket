package com.menusifu.cloudmenu.converter;

import com.menusifu.cloudmenu.dto.SaleItemDTO;
import com.menusifu.cloudmenu.entity.SaleItem;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author xie
 * @date 2019/1/15 13:43
 * @description:
 */
public class SaleItemAndSaleItemDTOConverter {
    public static SaleItemDTO converter(SaleItem saleItem) {
        SaleItemDTO saleItemDTO = new SaleItemDTO();
        if (saleItem == null) return null;
        BeanUtils.copyProperties(saleItem, saleItemDTO);
        return saleItemDTO;
    }

    public static List<SaleItemDTO> converter(List<SaleItem> saleItems) {
        if (saleItems == null) return null;
        return saleItems.stream().map(saleItem -> converter(saleItem)).collect(Collectors.toList());
    }

    public static SaleItem converter(SaleItemDTO saleItemDTO) {
        SaleItem saleItem = new SaleItem();
        if (saleItem == null) return null;
        BeanUtils.copyProperties(saleItemDTO, saleItem);
        return saleItem;
    }

    public static List<SaleItem> converter2SaleItemList(List<SaleItemDTO> saleItemDTOs) {
        if (saleItemDTOs == null) return null;
        return saleItemDTOs.stream().map(saleItemDTO -> converter(saleItemDTO)).collect(Collectors.toList());
    }
}
