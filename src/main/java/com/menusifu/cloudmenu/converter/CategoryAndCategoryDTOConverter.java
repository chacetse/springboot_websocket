package com.menusifu.cloudmenu.converter;

import com.menusifu.cloudmenu.dto.CategoryDTO;
import com.menusifu.cloudmenu.dto.SaleItemDTO;
import com.menusifu.cloudmenu.dto.TaxDTO;
import com.menusifu.cloudmenu.entity.Category;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author xie
 * @date 2019/1/15 13:42
 * @description:
 */
public class CategoryAndCategoryDTOConverter {
    public static CategoryDTO converter(Category category, List<SaleItemDTO> saleItems, TaxDTO tax) {
        CategoryDTO categoryDTO = new CategoryDTO();
        if (category == null) return null;
        BeanUtils.copyProperties(category, categoryDTO);
        categoryDTO.setSaleItems(saleItems);
        categoryDTO.setTax(tax);
        return categoryDTO;
    }

    public static List<CategoryDTO> converter(List<Category> categoryList, Map<String, List<SaleItemDTO>> saleItemMap, Map<String, TaxDTO> taxMap) {
        if (categoryList == null) return null;
        return categoryList.stream()
                .map(category ->
                        converter(category, saleItemMap.get(category.getId()), taxMap.get(category.getId()))).collect(Collectors.toList());
    }
}
