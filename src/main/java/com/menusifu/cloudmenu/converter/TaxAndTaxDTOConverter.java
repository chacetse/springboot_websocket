package com.menusifu.cloudmenu.converter;

import com.menusifu.cloudmenu.dto.TaxDTO;
import com.menusifu.cloudmenu.entity.Tax;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author xie
 * @date 2019/1/15 13:43
 * @description:
 */
public class TaxAndTaxDTOConverter {
    public static TaxDTO converter(Tax tax) {
        TaxDTO taxDTO = new TaxDTO();
        if (tax == null) return null;
        BeanUtils.copyProperties(tax, taxDTO);
        return taxDTO;
    }

    public static List<TaxDTO> converter(List<Tax> taxList) {
        if (taxList == null) return null;
        return taxList.stream().map(tax -> converter(tax)).collect(Collectors.toList());
    }
}
