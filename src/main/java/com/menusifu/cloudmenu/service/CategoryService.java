package com.menusifu.cloudmenu.service;

import com.menusifu.cloudmenu.dto.CategoryDTO;

import java.util.List;

/**
 * @author xie
 * @date 2019/1/15 14:48
 * @description:
 */
public interface CategoryService {
    List<CategoryDTO> getAllCategories();

    CategoryDTO addCategory(CategoryDTO Category);

    void updateCategory(CategoryDTO Category);

    void deleteCategory(String id);

    CategoryDTO getCategoryById(String id);

    CategoryDTO copyCategory(String id);

    List<CategoryDTO> getAllCategoriesByTaxId(String taxId);
}
