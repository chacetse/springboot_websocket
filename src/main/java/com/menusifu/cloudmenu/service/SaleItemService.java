package com.menusifu.cloudmenu.service;

import com.menusifu.cloudmenu.dto.SaleItemDTO;

import java.util.List;

/**
 * @author xie
 * @date 2019/1/9 17:18
 * @description:
 */
public interface SaleItemService {
    List<SaleItemDTO> getAllSaleItems();

    SaleItemDTO addSaleItem(SaleItemDTO saleItem);

    void updateSaleItem(SaleItemDTO saleItem);

    void deleteSaleItemById(String id);

    SaleItemDTO getSaleItemById(String id);

    List<SaleItemDTO> batchAddSaleItems(List<SaleItemDTO> saleItemDTOs);

    List<SaleItemDTO> getSaleItemsByCategoryId(String categoryId);

    void deleteSaleItemByCatId(String categoryId);
}
