package com.menusifu.cloudmenu.service;

import com.menusifu.cloudmenu.dto.TaxDTO;

import java.util.List;

/**
 * @author xie
 * @date 2019/1/15 14:20
 * @description:
 */
public interface TaxService {
    List<TaxDTO> getAllTaxes();

    TaxDTO addTax(TaxDTO Tax);

    void updateTax(TaxDTO Tax);

    void deleteTax(String id);

    TaxDTO getTaxById(String id);
}
