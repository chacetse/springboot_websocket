package com.menusifu.cloudmenu.service.impl;

import com.menusifu.cloudmenu.converter.TaxAndTaxDTOConverter;
import com.menusifu.cloudmenu.dao.TaxDao;
import com.menusifu.cloudmenu.dto.CategoryDTO;
import com.menusifu.cloudmenu.dto.TaxDTO;
import com.menusifu.cloudmenu.entity.Tax;
import com.menusifu.cloudmenu.service.CategoryService;
import com.menusifu.cloudmenu.service.TaxService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Date;
import java.util.List;

/**
 * @author xie
 * @date 2019/1/15 14:21
 * @description:
 */
@Service
public class TaxServiceImpl implements TaxService {
    @Autowired
    TaxDao taxDao;

    @Autowired
    CategoryService categoryService;

    @Override
    public List<TaxDTO> getAllTaxes() {
        return TaxAndTaxDTOConverter.converter(taxDao.getAllTaxes());
    }

    @Override
    public TaxDTO addTax(TaxDTO taxDTO) {
        Tax tax = new Tax();
//        taxDTO.setId(null);
        BeanUtils.copyProperties(taxDTO, tax);
        tax.setLastUpdated(new Date());
        return TaxAndTaxDTOConverter.converter(taxDao.addTax(tax));
    }

    @Override
    public void updateTax(TaxDTO taxDTO) {
        Tax tax = new Tax();
        BeanUtils.copyProperties(taxDTO, tax);
        tax.setLastUpdated(new Date());
        taxDao.updateTax(tax);
    }

    @Override
    public void deleteTax(String id) {
        List<CategoryDTO> byTaxId = categoryService.getAllCategoriesByTaxId(id);
        if (!CollectionUtils.isEmpty(byTaxId)) {
            throw new RuntimeException("This tax has been used by some category, can not be deleted!");
        }
        taxDao.deleteTax(id);
    }

    @Override
    public TaxDTO getTaxById(String id) {
        return TaxAndTaxDTOConverter.converter(taxDao.getTaxById(id));
    }
}
