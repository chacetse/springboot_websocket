package com.menusifu.cloudmenu.service.impl;

import com.menusifu.cloudmenu.converter.CategoryAndCategoryDTOConverter;
import com.menusifu.cloudmenu.dao.CategoryDao;
import com.menusifu.cloudmenu.dto.CategoryDTO;
import com.menusifu.cloudmenu.dto.SaleItemDTO;
import com.menusifu.cloudmenu.dto.TaxDTO;
import com.menusifu.cloudmenu.entity.Category;
import com.menusifu.cloudmenu.service.CategoryService;
import com.menusifu.cloudmenu.service.SaleItemService;
import com.menusifu.cloudmenu.service.TaxService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author xie
 * @date 2019/1/15 14:49
 * @description:
 */
@Service
public class CategoryServiceImpl implements CategoryService {
    @Autowired
    CategoryDao categoryDao;

    @Autowired
    SaleItemService saleItemService;

    @Autowired
    TaxService taxService;

    @Override
    public List<CategoryDTO> getAllCategories() {
        List<Category> categories = categoryDao.getAllCategories();
        Map<String, List<SaleItemDTO>> saleItemDTOListMap = new HashMap<>();
        Map<String, TaxDTO> taxMap = new HashMap<>();
        getConverterData(categories, saleItemDTOListMap, taxMap);
        return CategoryAndCategoryDTOConverter.converter(categories, saleItemDTOListMap, taxMap);
    }

    private void getConverterData(List<Category> categories, Map<String, List<SaleItemDTO>> saleItemDTOListMap, Map<String, TaxDTO> taxMap) {
        for (Category category : categories) {
            List<SaleItemDTO> saleItemsByCategoryId = saleItemService.getSaleItemsByCategoryId(category.getId());
            if (!CollectionUtils.isEmpty(saleItemsByCategoryId)) {
                saleItemDTOListMap.put(category.getId(), saleItemsByCategoryId);
            }
            if (category.getTaxId() != null) {
                TaxDTO taxById = taxService.getTaxById(category.getTaxId());
                taxMap.put(category.getId(), taxById);
            }
        }
    }

    @Override
    public CategoryDTO addCategory(CategoryDTO categoryDTO) {
        Category category = new Category();
        List<SaleItemDTO> saleItemDTOs = categoryDTO.getSaleItems();
//        if (!CollectionUtils.isEmpty(saleItemDTOs)) {
//            categoryDTO.setSaleItems(saleItemService.batchAddSaleItems(saleItemDTOs));
//        }
//        categoryDTO.setId(null);
        BeanUtils.copyProperties(categoryDTO, category);
        if (categoryDTO.getTax() != null && categoryDTO.getTax().getId() != null) {
            category.setTaxId(categoryDTO.getTax().getId());
        }
        category.setLastUpdated(new Date());
        Category dbCategory = categoryDao.addCategory(category);
        return CategoryAndCategoryDTOConverter.converter(dbCategory, saleItemDTOs, categoryDTO.getTax());
    }

    @Override
    public void updateCategory(CategoryDTO categoryDTO) {
        Category category = new Category();
        BeanUtils.copyProperties(categoryDTO, category);
        if (categoryDTO.getTax() != null && categoryDTO.getTax().getId() != null) {
            category.setTaxId(categoryDTO.getTax().getId());
        }
        category.setLastUpdated(new Date());
        categoryDao.updateCategory(category);
    }

    @Override
    public void deleteCategory(String id) {
        saleItemService.deleteSaleItemByCatId(id);
        categoryDao.deleteCategory(id);
    }

    @Override
    public CategoryDTO getCategoryById(String id) {
        List<SaleItemDTO> saleItemsByCategoryId = saleItemService.getSaleItemsByCategoryId(id);
        Category categoryById = categoryDao.getCategoryById(id);
        if (categoryById == null) {
            return null;
        }
        TaxDTO taxById = taxService.getTaxById(categoryById.getTaxId());
        return CategoryAndCategoryDTOConverter.converter(categoryById, saleItemsByCategoryId, taxById);
    }

    @Override
    public CategoryDTO copyCategory(String id) {
        CategoryDTO categoryDTO = getCategoryById(id);
        categoryDTO.setId(null);
        CategoryDTO resultCategoryDTO = addCategory(categoryDTO);
        List<SaleItemDTO> saleItemDTOs = saleItemService.getSaleItemsByCategoryId(id);
        List<SaleItemDTO> newSaleItemDTOs = saleItemDTOs.stream().map(saleItemDTO -> {
            saleItemDTO.setCategoryId(resultCategoryDTO.getId());
            saleItemDTO.setId(null);
            return saleItemDTO;
        }).collect(Collectors.toList());
        List<SaleItemDTO> resultSaleItemDTOs = saleItemService.batchAddSaleItems(newSaleItemDTOs);
        resultCategoryDTO.setSaleItems(resultSaleItemDTOs);
        return resultCategoryDTO;
    }

    @Override
    public List<CategoryDTO> getAllCategoriesByTaxId(String taxId) {
        List<Category> categories = categoryDao.getCategoriesByTaxId(taxId);
        if (CollectionUtils.isEmpty(categories)) {
            return null;
        }
        Map<String, List<SaleItemDTO>> saleItemDTOListMap = new HashMap<>();
        Map<String, TaxDTO> taxMap = new HashMap<>();
        getConverterData(categories, saleItemDTOListMap, taxMap);
        return CategoryAndCategoryDTOConverter.converter(categories, saleItemDTOListMap, taxMap);
    }
}
