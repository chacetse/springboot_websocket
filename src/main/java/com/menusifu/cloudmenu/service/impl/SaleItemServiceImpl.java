package com.menusifu.cloudmenu.service.impl;

import com.menusifu.cloudmenu.converter.SaleItemAndSaleItemDTOConverter;
import com.menusifu.cloudmenu.dao.SaleItemDao;
import com.menusifu.cloudmenu.dto.CategoryDTO;
import com.menusifu.cloudmenu.dto.SaleItemDTO;
import com.menusifu.cloudmenu.entity.SaleItem;
import com.menusifu.cloudmenu.service.CategoryService;
import com.menusifu.cloudmenu.service.SaleItemService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @author xie
 * @date 2019/1/9 17:21
 * @description:
 */
@Service
public class SaleItemServiceImpl implements SaleItemService {
    @Autowired
    SaleItemDao saleItemDao;

    @Autowired
    CategoryService categoryService;

    @Override
    public List<SaleItemDTO> getAllSaleItems() {
        return SaleItemAndSaleItemDTOConverter.converter(saleItemDao.getAllSaleItems());
    }

    @Override
    public SaleItemDTO addSaleItem(SaleItemDTO saleItemDTO) {
        SaleItem saleItem = new SaleItem();
        if (saleItemDTO.getCategoryId() == null) {
            throw new RuntimeException("Category id is empty!");
        }
        CategoryDTO categoryDTO = categoryService.getCategoryById(saleItemDTO.getCategoryId());
        if (categoryDTO == null) {
            throw new RuntimeException("Category is empty!");
        }
//        saleItemDTO.setId(null);
        BeanUtils.copyProperties(saleItemDTO, saleItem);
        saleItem.setLastUpdated(new Date());
        return SaleItemAndSaleItemDTOConverter.converter(saleItemDao.addSaleItem(saleItem));
    }

    @Override
    public void updateSaleItem(SaleItemDTO saleItemDTO) {
        SaleItem saleItem = new SaleItem();
        BeanUtils.copyProperties(saleItemDTO, saleItem);
        saleItem.setLastUpdated(new Date());
        saleItemDao.updateSaleItem(saleItem);
    }

    @Override
    public void deleteSaleItemById(String id) {
        saleItemDao.deleteSaleItemById(id);
    }

    @Override
    public SaleItemDTO getSaleItemById(String id) {
        return SaleItemAndSaleItemDTOConverter.converter(saleItemDao.getSaleItemById(id));
    }

    @Override
    public List<SaleItemDTO> batchAddSaleItems(List<SaleItemDTO> saleItemDTOs) {
        List<SaleItem> saleItems = SaleItemAndSaleItemDTOConverter.converter2SaleItemList(saleItemDTOs);
        return SaleItemAndSaleItemDTOConverter.converter(saleItemDao.batchAddSaleItems(saleItems));
    }

    @Override
    public List<SaleItemDTO> getSaleItemsByCategoryId(String categoryId) {
        return SaleItemAndSaleItemDTOConverter.converter(saleItemDao.getSaleItemsByCategoryId(categoryId));
    }

    @Override
    public void deleteSaleItemByCatId(String categoryId) {
        saleItemDao.deleteSaleItemByCatId(categoryId);
    }
}
