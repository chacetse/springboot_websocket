package com.menusifu.cloudmenu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author xie
 * @date 2019/1/9 15:03
 * @description:
 */
@SpringBootApplication
@EnableScheduling
public class CloudMenuApplication {

    public static void main(String[] args) {
        SpringApplication.run(CloudMenuApplication.class, args);
    }

}

