package com.menusifu.cloudmenu.web;

import com.menusifu.cloudmenu.dto.SaleItemDTO;
import com.menusifu.cloudmenu.service.SaleItemService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author xie
 * @date 2019/1/9 17:24
 * @description:
 */
@Api(value = "Merchant API", description = "menu sync sale item module")
@RestController
@RequestMapping("/cloudmenu/sale-item")
public class SaleItemController {
    @Autowired
    SaleItemService saleItemService;

    @GetMapping("/")
    @ApiOperation(value="get sale item list", notes="get sale item list")
    public List<SaleItemDTO> getAllSaleItems() {
        return saleItemService.getAllSaleItems();
    }

    @GetMapping("/{id}")
    @ApiOperation(value="get sale item by id", notes="get sale item by id")
    public SaleItemDTO getSaleItemById(@PathVariable("id") String id) {
        return saleItemService.getSaleItemById(id);
    }

    @PostMapping("/")
    @ApiOperation(value="add sale item", notes="add sale item")
    public SaleItemDTO addSaleItem(@RequestBody SaleItemDTO saleItem) {
        return saleItemService.addSaleItem(saleItem);
    }

    @PutMapping("/")
    @ApiOperation(value="update sale item", notes="update sale item")
    public void updateSaleItem(@RequestBody SaleItemDTO saleItem) {
        saleItemService.updateSaleItem(saleItem);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value="delete sale item by id", notes="delete sale item by id")
    public void deleteSaleItemById(@PathVariable("id") String id) {
        saleItemService.deleteSaleItemById(id);
    }
}
