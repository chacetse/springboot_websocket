package com.menusifu.cloudmenu.web;

import com.menusifu.cloudmenu.dto.CategoryDTO;
import com.menusifu.cloudmenu.service.CategoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author xie
 * @date 2019/1/15 14:53
 * @description:
 */
@Api(value = "Merchant API", description = "menu sync category module")
@RestController
@RequestMapping("/cloudmenu/category")
public class CategoryController {
    @Autowired
    CategoryService categoryService;

    @GetMapping("/")
    @ApiOperation(value = "get category list", notes = "get category list")
    public List<CategoryDTO> getAllCategories() {
        return categoryService.getAllCategories();
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "get category by id", notes = "get category by id")
    public CategoryDTO getCategoryById(@PathVariable("id") String id) {
        return categoryService.getCategoryById(id);
    }

    @PostMapping("/")
    @ApiOperation(value = "add category", notes = "add category")
    public CategoryDTO addCategory(@RequestBody CategoryDTO category) {
        return categoryService.addCategory(category);
    }

    @PutMapping("/")
    @ApiOperation(value = "update category", notes = "update category")
    public void updateCategory(@RequestBody CategoryDTO category) {
        categoryService.updateCategory(category);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "delete category by id", notes = "delete category by id")
    public void deleteCategoryById(@PathVariable("id") String id) {
        categoryService.deleteCategory(id);
    }

    @PostMapping("/copy/{id}")
    @ApiOperation(value = "copy category", notes = "copy category")
    public CategoryDTO copyCategory(@PathVariable("id") String id) {
        return categoryService.copyCategory(id);
    }
}
