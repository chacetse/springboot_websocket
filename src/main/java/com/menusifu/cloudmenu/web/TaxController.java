package com.menusifu.cloudmenu.web;

import com.menusifu.cloudmenu.dto.TaxDTO;
import com.menusifu.cloudmenu.service.TaxService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author xie
 * @date 2019/1/15 14:12
 * @description:
 */
@Api(value = "Merchant API", description = "menu sync tax module")
@RestController
@RequestMapping("/cloudmenu/tax")
public class TaxController {
    @Autowired
    TaxService taxService;

    @GetMapping("/")
    @ApiOperation(value="get tax list", notes="get tax list")
    public List<TaxDTO> getAllTaxes() {
        return taxService.getAllTaxes();
    }

    @GetMapping("/{id}")
    @ApiOperation(value="get tax by id", notes="get tax by id")
    public TaxDTO getTaxById(@PathVariable("id") String id) {
        return taxService.getTaxById(id);
    }

    @PostMapping("/")
    @ApiOperation(value="add tax", notes="add tax")
    public TaxDTO addTax(@RequestBody TaxDTO saleItem) {
        return taxService.addTax(saleItem);
    }

    @PutMapping("/")
    @ApiOperation(value="update tax", notes="update tax")
    public void updateTax(@RequestBody TaxDTO saleItem) {
        taxService.updateTax(saleItem);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value="delete tax by id", notes="delete tax by id")
    public void deleteTaxById(@PathVariable("id") String id) {
        taxService.deleteTax(id);
    }
}
