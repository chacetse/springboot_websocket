package com.menusifu.cloudmenu.dto;

import lombok.Data;

/**
 * @author xie
 * @date 2019/1/15 12:53
 * @description:
 */
@Data
public class SaleItemDTO {
    private String id;
    private String itemName;
    private Double price;
    private String description;
    private String thumbPath;
    private String lastUpdatedBy;
    private String printerNames;
    private String categoryId;
    private String frontEndId;
}
