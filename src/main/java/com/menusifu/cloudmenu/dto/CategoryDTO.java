package com.menusifu.cloudmenu.dto;

import lombok.Data;

import java.util.List;

/**
 * @author xie
 * @date 2019/1/15 12:52
 * @description:
 */
@Data
public class CategoryDTO {
    private String id;
    private String categoryName;
    private String description;
    private String lastUpdatedBy;
    private TaxDTO tax;
    private List<SaleItemDTO> saleItems;
    private String frontEndId;
}
