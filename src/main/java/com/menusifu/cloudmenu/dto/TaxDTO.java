package com.menusifu.cloudmenu.dto;

import lombok.Data;

/**
 * @author xie
 * @date 2019/1/15 12:53
 * @description:
 */
@Data
public class TaxDTO {
    private String id;
    private Double rate;
    private String name;
    private String description;
    private String frontEndId;
}
