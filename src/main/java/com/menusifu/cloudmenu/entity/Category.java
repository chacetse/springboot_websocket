package com.menusifu.cloudmenu.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * @author xie
 * @date 2019/1/15 11:06
 * @description:
 */
@Document(collection = "Category")
@Data
public class Category {
    @Id
    private String id;
    private String categoryName;
    private String description;
    private String lastUpdatedBy;
    private String taxId;
    private Date lastUpdated;
    private boolean deleted = false;
    private String frontEndId;
}
