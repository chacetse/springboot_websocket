package com.menusifu.cloudmenu.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * @author xie
 * @date 2019/1/15 11:08
 * @description:
 */
@Data
@Document(collection = "Tax")
public class Tax {
    @Id
    private String id;
    private Double rate;
    private String name;
    private String description;
    private Date lastUpdated;
    private boolean deleted = false;
    private String frontEndId;
}
