package com.menusifu.cloudmenu.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;


/**
 * @author xie
 * @date 2019/1/9 15:39
 * @description:
 */
@Document(collection = "SaleItem")
@Data
public class SaleItem {
    @Id
    private String id;
    private String itemName;
    private Double price;
    private String description;
    private String thumbPath;
    private String lastUpdatedBy;
    private String printerNames;
    private Date lastUpdated;
    private String categoryId;
    private boolean deleted = false;
    private String frontEndId;
}
